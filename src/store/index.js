import Vuex from 'vuex'
import Vue from 'vue'
import todos from './module/todos'

// Load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        todos
    }
});